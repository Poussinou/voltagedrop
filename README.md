Calculate voltage drop

Voltage Drop Calculator is an Android app which helps you calculate the voltage drop in electrical installations and stay within wiring code limits. It can either calculate the voltage drop from given line parameters, or suggest limits for current, wire gauge and length to keep the voltage drop within a given limit.

Copyright © 2021 Michael von Glasow

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/. 
